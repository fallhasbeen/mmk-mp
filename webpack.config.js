var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  devtool: 'eval',
  entry: {
      bundle:'./src/index',
      vendor: ['react','react-router','react-dom','mobx','mobx-react','react-weui']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    // publicPath: '/'
  },
  plugins: [
    new ExtractTextPlugin("index.css"),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.min.js'),
    new webpack.DefinePlugin({
        "process.env": {
          NODE_ENV: JSON.stringify("production")
        }
    }),
    // new webpack.DefinePlugin({
    //     'process.env.NODE_ENV': '"development"'
    // }),
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    })
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {test: /\.jsx?$/,loaders: ['babel'],include: path.join(__dirname, 'src')},
      {test: /\.css$/,loader: ExtractTextPlugin.extract("style","css")},
      {test: /\.less$/,loader: ExtractTextPlugin.extract('style','css!less')},
      {test: /\.(jpg|png)$/, loader: "url?limit=8192"}
    ]
  }
}
