var util={};
(function() {
  util.regPhone=/^(\d{3,4}[-－]?)?\d{7,8}$|^(\d{3,4}[-－]\d{7,8})$|^(13|15|17|18|14)[0-9]{9}$/;
  util.regName=/^[\u4E00-\u9FA5]{2,5}$/;
  util.regSign=new RegExp("[`~!@#$^&%*()=+|{}':;',\\[\\].<>/?~！@#￥……*（）&;—|{}【】‘；：”“'。，、？]");
  util.setLocalData=function(key, val) {
      util.removeLocalData(key);
      if (window.localStorage) {
          window.localStorage.setItem(key, JSON.stringify(val));
      } else {
          util.setCookie(key, val);
      }
  };
  util.removeLocalData=function(key) {
      if (window.localStorage) {
          window.localStorage.removeItem(key);
      } else {
          util.delCookie(key);
      }
  };
  util.getLocalData=function(key) {
      var val = "";
      if (window.localStorage) {
          val = window.localStorage.getItem(key);
      } else {
          val = util.getCookie(key);
      }
      if (!val || val == "undefined") return "";
      return JSON.parse(val);
  };

  util.setCookie=function(name, value, exp) {
      var expDate = new Date();
      if (typeof(exp) == "number") {

          expDate.setTime(expDate.getTime() + (exp));
      } else if (Object.prototype.toString.call(exp) == "[object Date]") {
          expDate = exp;
      } else {
          console.error('exp must be number or date');
      }
      document.cookie = name + "=" + escape(value) + ";expires=" + expDate.toUTCString();
  };
  util.getCookie=function(name) {

      var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
      if (arr = document.cookie.match(reg))
          return (arr[2]);
      else
          return null;
  };
  util.delCookie=function(name) {

      var exp = new Date();
      exp.setTime(exp.getTime() - 1);
      var cval = util.getCookie(name);
      if (cval != null)
          document.cookie = name + "=" + cval + ";expires=" + exp.toUTCString();
  }

})();
