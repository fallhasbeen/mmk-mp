import React, { Component } from 'react';
import {Link} from 'react-router';
import { observer } from 'mobx-react';
import {Form,FormCell,CellBody,TextArea,Input,Button,CellFooter,Select,Dialog} from 'react-weui';
const {Alert, Confirm} = Dialog;

import dingwei from '../../images/icon_dingwei.png';

import './addReceiverInfo.less';


@observer(["RegionStores"]) class EditReceiverInfo extends Component{
  constructor(props){
    super(props)
    this.state={
      receiverArea:'',
      name:'',
      phone:'',
      address:'',
      addressInfo:'',
      showAlert  : false,
      textAlert  : '',
      alert      : {
          title  : '请完善收件人信息',
          buttons: [
              {
                  label  : '好的',
                  onClick: this.hideAlert.bind(this)
              }
          ]
      },
      confirm    : {
          title  : '是否确定删除该联系人',
          buttons: [
              {
                  type   : 'default',
                  label  : '取消',
                  onClick: this.hideConfirm.bind(this)
              },
              {
                  type   : 'primary',
                  label  : '确定',
                  onClick: this.deleteReceiverData.bind(this)
              }
          ]
      }
    }
  }

  componentDidMount() {
    if(this.props.RegionStores.editReceiverState){
      let receiverInfo=this.props.RegionStores.editReceiverState;
      this.setState({
        name:receiverInfo.name,
        phone:receiverInfo.phone,
        address:receiverInfo.address,
        addressInfo:receiverInfo.addressInfo,
        receiverArea:receiverInfo.receiverArea
      })
    }
    if(this.props.RegionStores.regionState.area){
      let regions=this.props.RegionStores.regionState;
      let area =regions.province.name+regions.city.name+regions.area.name
      this.setState({receiverArea:area})
    }
  }

  hideAlert() {
      this.setState({showAlert: false});
  }
  hideConfirm() {
      this.setState({showConfirm: false});
  }

  editInfo(value,e){
    switch (value) {
      case 'name':
      let nameValue=e.target.value.replace(/(^\s*)|(\s*$)/g, "");
      this.setState({name:nameValue})
      break;
      case 'phone':
      let phoneValue=e.target.value.replace(/(^\s*)|(\s*$)/g, "");
      this.setState({phone:phoneValue})
      break;
      case 'address':
      let addressV=e.target.value.replace(/(^\s*)|(\s*$)/g, "");
      this.setState({address:addressV})
      break;
      case 'addressInfo':
      let addressValue=e.target.value.replace(util.regSign,'');
      this.setState({addressInfo:addressValue})
      break;
    }
  }

  editRegion(){
    let params={};
    const data=this.state;
    if(data.name){params.name=data.name};
    if(data.phone){params.phone=data.phone};
    if(data.address){params.address=data.address};
    if(data.addressInfo){params.addressInfo=data.addressInfo};
    this.props.RegionStores.setEditReceiver(params);
    this.props.history.push('sendReveicer/edit/region')
  }

  savedReceiver(){
    let params={};
    const data=this.state;
    params.name=data.name;
    params.phone=data.phone;
    params.address=data.address;
    params.receiverArea=data.receiverArea;
    if(this.saveReceiverData(params)){
      params.receiver=this.props.RegionStores.regionState;
      let receiverValue=util.getLocalData('saved-receivers');
      if(!receiverValue){
        receiverValue=[];
        receiverValue.push(params);
      }else{
        let receiverInfo=this.props.RegionStores.editReceiverState;
        receiverValue[receiverInfo.index]=params;
      }
      util.setLocalData('saved-receivers',receiverValue);
      util.setLocalData('indexReceiver',params);
      this.props.RegionStores.setReceiver(null);
      this.props.RegionStores.setRegion({})
      this.props.history.push('/');
    }
  }

  saveReceiverData(receiver){
    if(!receiver.name){
      this.setState({
        showAlert: true,
        textAlert: "收件人姓名不能为空"
      });
      return false;
    }

    if(!util.regPhone.test(receiver.phone)){
      this.setState({
        showAlert: true,
        textAlert: "请输入正确的手机或座机号码"
      });
      return false;
    }

    if(!receiver.receiverArea){
      this.setState({
        showAlert: true,
        textAlert: "收件人省市区不能为空"
      });
      return false;
    }

    if(!receiver.address){
      this.setState({
        showAlert: true,
        textAlert: "收件人详细地址不能为空"
      });
      return false;
    }
    return true;
  }

  linkGoBack(){
    this.props.RegionStores.setEditReceiver(null);
    this.props.RegionStores.setRegion({})
    this.props.history.push('sendReveicer');
  }

  deleteReceiver() {
      this.setState({showConfirm:true});
  }

  deleteReceiverData() {
      let savedReceivers = util.getLocalData('saved-receivers');
      let receiverInfo=this.props.RegionStores.editReceiverState;
      if (savedReceivers) {
          savedReceivers.splice(receiverInfo.index,1);
          util.setLocalData('saved-receivers',savedReceivers)
          this.props.history.push('sendReveicer');
      }
  }

  render(){
    return(
      <div>
        <div data-flex='dir:left box:justify' className='area-nav'>
          <span onClick={this.linkGoBack.bind(this)}>取消</span>
          <span style={{textAlign:'center'}}>收件人信息</span>
          <span onClick={this.deleteReceiver.bind(this)}>删除</span>
        </div>
        <Form>
          <FormCell>
            <CellBody>
              <Input type="text" onChange={this.editInfo.bind(this,'name')}
                value={this.state.name} placeholder="请输入收件人姓名"/>
            </CellBody>
          </FormCell>
          <FormCell >
            <CellBody>
              <Input type="tel" onChange={this.editInfo.bind(this,'phone')}
                value={this.state.phone}  placeholder="请输入收件人手机号码"/>
            </CellBody>
          </FormCell>
          <FormCell >
            <CellBody onClick={this.editRegion.bind(this)}>
              <Input type="text" placeholder="请选择收件人省市区" value={this.state.receiverArea} disabled/>
            </CellBody>
            <CellFooter>
              <img src={dingwei} style={{height:'1rem'}}/>
            </CellFooter>
          </FormCell>
          <FormCell >
            <CellBody>
              <Input type="text" onChange={this.editInfo.bind(this,'address')}
                value={this.state.address} placeholder="详细地址(请详细到门牌，楼号及房间号)"/>
            </CellBody>
          </FormCell>
          <FormCell>
            <CellBody>
              <TextArea onChange={this.editInfo.bind(this,'addressInfo')} showCounter={false}
                value={this.state.addressInfo} placeholder="长按可将地址信息粘贴于此处进行编辑，粘贴，剪切到上方的对应信息栏中"
                rows="4"></TextArea>
            </CellBody>
          </FormCell>
        </Form>
        <div className='addReceiver-footer'>
          <Button type="primary" onClick={this.savedReceiver.bind(this)}>确定</Button>
        </div>
        <Alert
            show={this.state.showAlert}
            title={this.state.alert.title}
            buttons={this.state.alert.buttons}>
            {this.state.textAlert}
        </Alert>
        <Confirm
            show={this.state.showConfirm}
            title={this.state.confirm.title}
            buttons={this.state.confirm.buttons}>
        </Confirm>
      </div>
    )
  }
}
export default EditReceiverInfo;
