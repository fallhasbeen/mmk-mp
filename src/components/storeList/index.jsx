import React, { Component } from 'react';
import {Link} from 'react-router';
import { observer } from 'mobx-react';
import {SearchBar,Toast} from 'react-weui';
import './store.less';

@observer(["RegionStores"]) class StoreList extends Component {
  constructor(props){
    super(props)
    this.state={
      data:[],
      showLoading:false
    }
  }
  componentDidMount() {
    this.setState({showLoading:true})
    mw.getStoreList((res)=>{
      this.setState({data:res})
      this.setState({showLoading:false})
    })
  }

  handleChange(text) {
      this.state.filter = text;
      this.setState(this.state);
  }
  storeInfo(store){
    this.props.RegionStores.setSenderStore(store);
    this.props.history.goBack();
  }

  render(){
    const{data}=this.state;
    let list=[]
    if(data.length!=0){
      data.map((s,idx)=>{
        list.push(<StoreItems key={s.sid} data={s} idx={idx} filter={this.state.filter} storeInfo={this.storeInfo.bind(this)}/>)
      })
    }
    return(
      <div>
        <div className='store-header'>
          <div data-flex='dir:left box:first' className='area-nav'>
            <Link to={`sendSender`}><span>取消</span></Link>
            <span style={{paddingLeft:'30%'}}>选择门店地址</span>
          </div>
          <SearchBar onChange={this.handleChange.bind(this)}/>
        </div>
        <div className='store-body'>
          {list}
        </div>
        <Toast icon="loading" show={this.state.showLoading}>
          加载中...
        </Toast>
      </div>
    )
  }
}
class StoreItems extends Component {
  constructor(props){
    super(props)
  }
  handleClick(){
    this.props.storeInfo(this.props.data);
  }
  render(){
    const {data,filter,idx}=this.props;
    let styles = {};
    if (filter && (data.storename.indexOf(filter) < 0)) {
        styles.display = 'none'
    };
    if(idx==0){
      styles.backgroundColor='#fa982e';
      styles.color='#fff';
    }
    return(
      <div className='store-item' style={styles} onClick={this.handleClick.bind(this)}>
        <p>{data.storename}</p>
        <p>{data.phone}</p>
        <p>{data.address}</p>
      </div>
    )
  }
}

export default StoreList;
