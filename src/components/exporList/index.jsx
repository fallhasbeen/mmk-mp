import React, { Component } from 'react';
import {Link,IndexLink} from 'react-router';
import {Toast} from 'react-weui';
import './exporList.less';

class ExporListItem extends Component{

  isCheck(data){
    this.props.isCheck(data)
  }

  render(){
    const {data}=this.props
    return(
      <div  key={data.eid} className='exporlist-box'
        data-flex='main:center cross:center box:first' onClick={this.isCheck.bind(this,data)}>
        <div><img className='exporlist-img' src={data.headPicUrl}/></div>
        <div style={{paddingLeft:'1rem'}}>
          <div>{data.nickname}</div>
          <div>{data.phone}</div>
        </div>
      </div>
    )
  }
}

class ExporList extends Component{
  state={
    expor:[],
    showLoading:false
  }

  componentWillMount() {
    this.setState({showLoading:true})
    mw.getExporList((res)=>{
      this.setState({expor:res})
      this.setState({showLoading:false})
    })
  }
  isCheck(data){
    util.setLocalData('saved-expor',data);
    this.props.history.push('/');
  }
  render(){
    let list=[];
    if(this.state.expor.length==0){
      list.push(<div>加载中。。。</div>)
    }else {
      this.state.expor.map((r,idx)=>{
        list.push(<ExporListItem key={idx} data={r} isCheck={this.isCheck.bind(this)}/>)
      })
    }
    return(
      <div>
        <div data-flex='dir:left box:first' className='area-nav'>
          <IndexLink to={`/`}><span>取消</span></IndexLink>
          <span style={{paddingLeft:'30%'}}>选择快递小哥</span>
        </div>
        <div>
          {list}
        </div>
        <Toast icon="loading" show={this.state.showLoading}>
          加载中...
        </Toast>
      </div>
    )
  }
}


export default ExporList;
