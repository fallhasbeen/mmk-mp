import { observable } from 'mobx';

class RegionState {
  @observable tabState = 0;
  @observable regionState = {};
  @observable storeState = {};
  @observable receiverState = null;
  @observable editReceiverState = null;
  @observable senderState = null;

  setTabState(idx){
    this.tabState=idx;
  }
  setRegion(area){
    this.regionState=area;
  }
  setReceiver(info){
    this.receiverState=info;
  }
  setEditReceiver(info){
    this.editReceiverState=info;
  }
  setSenderState(info){
    this.senderState=info;
  }
  setSenderStore(info){
    this.storeState=info;
  }
}

const regionState = new RegionState();
export default regionState;
export {RegionState};
